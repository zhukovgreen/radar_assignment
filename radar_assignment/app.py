import asyncio
import os
import pathlib

from concurrent.futures.process import ProcessPoolExecutor
from concurrent.futures.thread import ThreadPoolExecutor
from contextlib import contextmanager
from functools import partial
from typing import (
    Any,
    Callable,
    Iterator,
    List,
    Optional,
    Tuple,
    TypeVar,
    Union,
)

import numpy as np
import pandas as pd

from envparse import env
from loguru import logger

from .pkg_utils import get_title_description, get_version
from .setup_logger import setup_logger


env.read_envfile()
DEBUG = env.bool("DEBUG", default=False)


executor_r_type = TypeVar("executor_r_type")


async def _run_in_executor(
    executor: Union[ProcessPoolExecutor, ThreadPoolExecutor],
    func: Callable[[Any], executor_r_type],
    args: tuple = (),
    kwargs: Optional[dict] = None,
    prefix: str = "",
) -> executor_r_type:
    """Simple wrapper for conveniently running sync code in the executor."""
    if not kwargs:
        kwargs = {}
    logger.debug(f"{prefix}: Running in executor func: {repr(func)}")
    result = await asyncio.get_event_loop().run_in_executor(
        executor,
        partial(func, *args, **kwargs),
    )
    logger.debug(f"{prefix}: Execution completed.")
    return result


async def main_view(self: "RadarApp") -> dict:
    """Main view of the application."""
    logger.info("Preparing the environment...")
    await self.prepare_environment()
    logger.info("Preparing the environment finished.")
    process_text_stream = asyncio.create_task(self.text_stream.join())
    process_result_stream = asyncio.create_task(self.result_stream.join())
    await asyncio.wait(
        [process_text_stream, *self.preprocess_workers],
        return_when=asyncio.FIRST_COMPLETED,
    )
    await asyncio.wait(
        [process_result_stream, *self.postrocess_workers],
        return_when=asyncio.FIRST_COMPLETED,
    )
    if not (process_text_stream.done() or process_result_stream.done()):
        for worker in (*self.preprocess_workers, *self.postrocess_workers):
            if worker.done():
                worker.result()
    return await self.finalization()


def _preprocess_log_chunk(chunk: pd.DataFrame) -> pd.DataFrame:
    """Data preprocess worker.

    Creates pandas dataframe representation of the data.
    """
    # TODO: solve the creation of the df in more simple and faster way
    #   Now it is the bottleneck of the app.
    exp_clm = chunk["HOSTNAME+MSG_TAG+MSG_CONTENT"].str.extract(
        r"(?P<HOSTNAME>\S+) (?P<MSG_TAG>\S*?)\W (?P<MSG>.+)"
    )
    df = pd.concat(
        [chunk.drop("HOSTNAME+MSG_TAG+MSG_CONTENT", axis=1), exp_clm],
        axis=1,
    )
    df[["TIMESTAMP"]] = pd.to_datetime(df["TIMESTAMP"], format="%b %d %X")
    df["SEVERITY"] = df["PRI"] % 8
    return df


def _postprocess_log_chunk(
    chunk: pd.DataFrame,
) -> np.array:
    """Compute required data from the chunk.

    Then we aggregate this data in order to get the needed results.
    """
    average_msg_len = chunk["MSG"].str.len().mean()
    total_emergency = (chunk["SEVERITY"] == 0).sum()
    total_alert = (chunk["SEVERITY"] == 1).sum()
    max_timestamp = chunk["TIMESTAMP"].max()
    return np.array(
        (
            average_msg_len,
            total_emergency,
            total_alert,
            max_timestamp,
        )
    )


class RadarApp:
    """Basic Application class.

    The design assumes registering additional views (tasks). The design is in
    WIP state.
    """

    def __init__(
        self,
        path_to_log: pathlib.Path,
        debug: bool = False,
        chunk_size: int = None,
        num_workers: int = None,
    ) -> None:
        self.path_to_log: pathlib.Path = path_to_log
        self.num_workers: int = num_workers or env.int(
            "NUM_WORKERS", default=os.cpu_count()
        )
        self.process_pool: ProcessPoolExecutor = ProcessPoolExecutor(
            max_workers=self.num_workers
        )
        self.chunk_size: int = chunk_size or env.int(
            "CHUNK_SIZE", default=10000
        )
        self.text_stream: asyncio.Queue[
            Tuple[int, pd.DataFrame]
        ] = None  # type: ignore
        self.result_stream: asyncio.Queue[
            Tuple[int, pd.DataFrame]
        ] = None  # type: ignore
        self.preprocess_workers: List[asyncio.Task] = []
        self.postrocess_workers: List[asyncio.Task] = []
        self._views: List[Callable[["RadarApp"], Any]] = []
        self.views: List[asyncio.Task] = []
        self.debug: bool = debug
        self.result_chunks: List[np.array] = []
        self.results: np.array = None

    async def _preprocess_worker(self, worker_id: int) -> None:
        """Worker that preprocessing the chunk of the data.

        The worker is always running, until the app will close the event loop.

        It takes the data from the text_stream (stream of chunks), preprocess
        the data and put the results in the result_stream.
        """
        logger.info(f"Preprocess worker id {worker_id} started.")
        while True:
            logger.debug(f"Text stream length: {self.text_stream.qsize()}")
            chunk_id, chunk = await self.text_stream.get()
            logger.info(
                f"Preprocess worker id {worker_id} started processing "
                f"chunk id {chunk_id}..."
            )

            preprocessed_chunk = await _run_in_executor(
                executor=self.process_pool,
                func=_preprocess_log_chunk,
                args=(chunk,),
                prefix=f"PreP Worker {worker_id}",
            )
            self.text_stream.task_done()
            await self.result_stream.put((chunk_id, preprocessed_chunk))
            logger.info(
                f"Chunk {chunk_id} processed by preprocess worker {worker_id}"
            )

    async def _postprocess_worker(self, worker_id: int) -> None:
        """Worker that postprocessing the preprocessed chunks.

        The worker is always running, until the app will close the event loop.

        It takes the data from the result_stream, postprocess it and appends
        into self.result_chunks list.
        """
        logger.info(f"Postprocess worker id {worker_id} started.")
        while True:
            logger.debug(f"Result stream length: {self.result_stream.qsize()}")
            chunk_id, chunk = await self.result_stream.get()
            logger.info(
                f"Postprocess worker id {worker_id} started processing"
                f" chunk id {chunk_id}..."
            )
            self.result_chunks.append(
                await _run_in_executor(
                    executor=self.process_pool,
                    func=_postprocess_log_chunk,
                    args=(chunk,),
                    prefix=f"PostP Worker {worker_id}",
                )
            )

            self.result_stream.task_done()
            logger.info(
                f"Chunk {chunk_id} processed by postprocess worker {worker_id}"
            )

    async def finalization(self) -> dict:
        """Finilize the data in the self.result_chunks.

        Method creates the aggregated dict representation
        of the data.
        """
        result_chunks_array = np.array(self.result_chunks)
        return {
            "average_msg_len": result_chunks_array[:, 0].mean(),
            "total_emergency": result_chunks_array[:, 1].sum(),
            "total_alert": result_chunks_array[:, 2].sum(),
            "max_timestamp": result_chunks_array[:, 3].max(),
        }

    async def prepare_environment(self) -> None:
        """Prepare the app envirnoment.

        Split the data in the log file into chunks (Iterator).
        Put them into the processing queue and spin up workers.
        """
        logger.info("Preparing the app environment...")
        for chunk_id, chunk in enumerate(
            pd.read_csv(
                self.path_to_log,
                sep=r"[<>]|(?<=\d{2}:\d{2}:\d{2}) ",
                engine="python",
                names=(
                    "A",
                    "PRI",
                    "TIMESTAMP",
                    "HOSTNAME+MSG_TAG+MSG_CONTENT",
                ),
                chunksize=self.chunk_size,
            )
        ):
            await self.text_stream.put((chunk_id, chunk))
            logger.debug(f"Chunk id {chunk_id:2} added to the queue.")
        for worker_id in range(self.num_workers):
            self.preprocess_workers.append(
                asyncio.create_task(
                    coro=self._preprocess_worker(worker_id),
                    name=f"Preprocess worker: {worker_id}",
                )
            )
            logger.info(f"Preprocess worker id {worker_id:2} was initialized")
            self.postrocess_workers.append(
                asyncio.create_task(
                    coro=self._postprocess_worker(worker_id),
                    name=f"Postprocess worker: {worker_id}",
                )
            )
            logger.info(f"Postprocess worker id {worker_id:2} was initialized")

    def register(self, view: Callable[["RadarApp"], Any]) -> None:
        """Register the app view.

        Should take any
        """
        self._views.append(view)
        logger.info(f"View {view} registered in the app.")

    async def execute(self) -> None:
        """App async entrypoint to execute all views and get the results."""
        self.text_stream = asyncio.Queue()
        self.result_stream = asyncio.Queue()
        for view in self._views:
            self.views.append(asyncio.create_task(coro=view(self)))
        self.results = await asyncio.gather(*self.views)
        for t in (*self.preprocess_workers, *self.postrocess_workers):
            not t.done() and t.cancel()

    @contextmanager
    def serve(self) -> Iterator["RadarApp"]:
        """Context Manager to start serving the app.

        It runs self.app.tear_down on exit.
        """
        logger.info("Application started serving...")
        asyncio.run(self.execute(), debug=self.debug)
        try:
            yield self
        finally:
            self.app_teardown()

    def app_teardown(self) -> None:
        """Sync app teardown routine."""
        logger.info("Application teardown started...")
        # some teardown routine could be here
        logger.info("Application teardown completed...")


def build_app(
    file: pathlib.Path,
    debug: bool = False,
    chunk_size: Optional[int] = None,
    num_workers: Optional[int] = None,
) -> RadarApp:
    """Build the application."""
    version = get_version()
    title = get_title_description()
    setup_logger(debug=debug or DEBUG)
    logger.info("Welcome to the application {}. {}".format(*title))

    logger.info(f"Building application version {version} started...")
    app = RadarApp(
        path_to_log=file,
        debug=debug or DEBUG,
        chunk_size=chunk_size,
        num_workers=num_workers,
    )

    logger.info("Registering views...")
    app.register(main_view)
    logger.info("Registering views finished.")

    logger.info("Building application finished.")
    return app
