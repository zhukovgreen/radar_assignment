import pathlib

import click

from radar_assignment.app import build_app


@click.command()
@click.argument("file", type=click.Path(exists=True))
@click.option("-v", "--verbose", count=True, help="Verbosity of the tool")
def cli(verbose: bool, file: pathlib.Path) -> None:
    """Application to do something."""
    # TODO Adjust the docstring
    app = build_app(file=pathlib.Path(file), debug=verbose)
    with app.serve() as app:
        results = app.results
    click.echo(results)
