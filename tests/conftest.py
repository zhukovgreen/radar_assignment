from pathlib import Path

import pytest

from radar_assignment.app import RadarApp, build_app


@pytest.fixture()
def app() -> RadarApp:
    """Fixture to expose already built app instance."""
    return build_app(file=Path(__file__).parent / "data-test.txt", debug=True)
