# radar-assignment

Assignment given during the recruiting process

[![asciicast](https://asciinema.org/a/DfYBZ17IKXVAyaivO9veAF5KS.png)](https://asciinema.org/a/DfYBZ17IKXVAyaivO9veAF5KS)

## Development

### Required dependencies

- **poetry** - (https://python-poetry.org/docs/#installation) and
- **pre-commit** - (https://pre-commit.com/#install)

Copy environment file and adjust based on your needs:
```bash
cp .env.example .env
```

### Create development environment

```bash
make install
```

### Running tests and linters

```bash
make test
```


## Profiling analysis

### The speed as a function of NUM_WORKERS
![](.num_workers_profiling.png)

### The speed as a function of CHUNK_SIZE
![](.chunk_size_profiling.png)

### Recomendations

Take CHUNK_SIZE roughly ~10_000, 20_000

Take NUM_WORKERS = as number of CPU - 1
